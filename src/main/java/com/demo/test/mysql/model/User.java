package com.demo.test.mysql.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GenericGenerator(name = "user_id", strategy = "com.demo.test.mysql.util.UuidGenerator")
    @GeneratedValue(generator = "user_id")
    private String userId;
    private String firstName;
    private String lastName;

    @NotNull(message = "Age cannot be null")
    private Integer age;

    @NotNull(message = "Phone Number cannot be null")
    @Size(min = 10, max = 13, message = "Invalid length for phone number")
    private String phoneNumber;

    @NotNull(message = "Password cannot be null")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Roles> roles = new HashSet<>();

    public User() {}

    public User(String firstName, String lastName, Integer age,
                String phoneNumber, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }
}
