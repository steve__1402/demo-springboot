package com.demo.test.mysql.service;

import com.demo.test.mysql.exception.RefreshToken.RefreshTokenException;
import com.demo.test.mysql.model.RefreshToken;
import com.demo.test.mysql.model.User;
import com.demo.test.mysql.repository.RefreshTokenRepository;
import com.demo.test.mysql.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Value("${demo.app.jwtRefreshExpirationMs}")
    private Long refreshTokenDuration;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UsersRepository userRepository;

    @Autowired
    private UsersService usersService;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken generateRefreshToken(String username){
        User user = userRepository.findById(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setUser(user);
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDuration));
        refreshToken.setToken(UUID.randomUUID().toString());
        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new RefreshTokenException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }

        return token;
    }

    private List<User> getIdUser(String phoneNumber) {
        List<User> tempUser = userRepository.findIdByPhoneNumber(phoneNumber);
        return tempUser;
    }
}
