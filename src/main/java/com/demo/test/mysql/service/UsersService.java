package com.demo.test.mysql.service;

import com.demo.test.mysql.model.User;
import com.demo.test.mysql.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public List<User> allUsers() {
        return usersRepository.findAll();
    }

    public void saveUsers(User user) {
        usersRepository.save(user);
    }

    public void updateUser(User user) {
        usersRepository.save(user);
    }

    public Optional<User> getUsers(String id) {
        return usersRepository.findById(id);
    }

    public List<User> getIdUser (String phoneNumber){
        return usersRepository.findIdByPhoneNumber(phoneNumber);
    }

    public void deleteUser(String phoneNumber) {
        usersRepository.deleteByPhoneNumber(phoneNumber);
    }
}
