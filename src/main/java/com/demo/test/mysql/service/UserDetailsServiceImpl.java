package com.demo.test.mysql.service;

import com.demo.test.mysql.model.User;
import com.demo.test.mysql.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UsersRepository usersRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        User user = usersRepository.findById(phoneNumber)
                .orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
        return UserDetailsImpl.build(user);
    }

}
