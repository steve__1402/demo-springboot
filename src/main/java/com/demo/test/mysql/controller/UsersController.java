package com.demo.test.mysql.controller;

import com.demo.test.mysql.model.EnumRoles;
import com.demo.test.mysql.model.Roles;
import com.demo.test.mysql.model.User;
import com.demo.test.mysql.payload.request.RegistrationRequest;
import com.demo.test.mysql.repository.RoleRepository;
import com.demo.test.mysql.repository.UsersRepository;
import com.demo.test.mysql.service.UsersService;
import com.demo.test.mysql.util.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping("/api/users")
public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> list() {
        return usersService.allUsers();
    }

    @GetMapping("/{phoneNumber}")
    @PreAuthorize("hasRole('ADMIN')")
    public Object get(@PathVariable String phoneNumber) {
        List<User> tempUser = usersService.getIdUser(phoneNumber);
        if (tempUser.size() == 0) {
            return ResponseHandler.generateResponse(
                    "User with phone number " + phoneNumber + " is not exist",
                    HttpStatus.NOT_FOUND,
                    null
            );
        }
        return ResponseHandler.generateResponse(
                "Successfully retrieved data!",
                HttpStatus.OK,
                tempUser
        );
    }

    @GetMapping(
            path = "/data",
            consumes = {
                    MediaType.APPLICATION_FORM_URLENCODED_VALUE,
                    MediaType.MULTIPART_FORM_DATA_VALUE
            },
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasRole('ADMIN')")
    public Object getUser(@RequestBody @ModelAttribute User user) {
        List<User> tempUser = usersService.getIdUser(user.getPhoneNumber());
        if (tempUser.size() > 0) {
            return ResponseHandler.generateResponse(
                    "Successfully get data user!",
                    HttpStatus.OK,
                    tempUser
            );
        }
        return ResponseHandler.generateResponse(
                "User with " + user.getPhoneNumber() + " is not exist",
                HttpStatus.NOT_FOUND,
                null
        );

    }

    @PostMapping("/addUser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> add(@RequestBody @Validated User user) {
        List<User> data = checkUser(user.getPhoneNumber());
        if (data.size() > 0) {
            return ResponseHandler.generateResponse(
                    "Duplicate user ",
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    null
            );
        }
        try {
            passwordEncoder = new BCryptPasswordEncoder();
            String encodedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            usersService.saveUsers(user);
            return ResponseHandler.generateResponse(
                    "Successfully create user ",
                    HttpStatus.OK,
                    mappingResponse(user)
            );
        } catch (NoSuchElementException e) {
            return ResponseHandler.generateResponse(
                    e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    null
            );
        }
    }

    @PostMapping(
            path = "/add",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> addUser(@RequestBody @ModelAttribute @Validated RegistrationRequest request, BindingResult bindingResult) {
        try {
            if (usersRepository.existsByPhoneNumber(request.getPhoneNumber())) {
                return ResponseHandler.generateResponse(
                        "Phone number is already use",
                        HttpStatus.BAD_REQUEST,
                        null
                );
            }

            if (bindingResult.hasErrors()) {
                Map<String, Object> body = new HashMap<>();
                body.put("timestamp", new Date());
                body.put("status", "error");

                //Get all errors
                List<String> errors = bindingResult.getAllErrors()
                        .stream()
                        .map(x -> x.getDefaultMessage())
                        .collect(Collectors.toList());

                body.put("errors", errors);

                return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);

            }
            User user = new User(
                    request.getFirstName(),
                    request.getLastName(),
                    request.getAge(),
                    request.getPhoneNumber(),
                    passwordEncoder.encode(request.getPassword())
            );

            //Check roles value from request
            Set<String> strRoles = request.getRole();
            user.setRoles(checkRole(strRoles));
            usersRepository.save(user);
            return ResponseHandler.generateResponse(
                    "Successfully create user ",
                    HttpStatus.OK,
                    mappingResponse(user)
            );
        } catch (NoSuchElementException e) {
            return ResponseHandler.generateResponse(
                    e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    null
            );
        }
    }

    @DeleteMapping("/delete/{phoneNumber}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> delete(@PathVariable String phoneNumber) {
        try {
            List<User> tempUser = checkUser(phoneNumber);
            if (tempUser.size() == 0) {
                return ResponseHandler.generateResponse(
                        "User with phone number " + phoneNumber + " is not exist",
                        HttpStatus.NOT_FOUND,
                        null
                );
            }
            usersService.deleteUser(phoneNumber);
            return ResponseHandler.generateResponse(
                    "Successfully delete user " + phoneNumber,
                    HttpStatus.OK,
                    null
            );
        } catch (NoSuchElementException e) {
            return ResponseHandler.generateResponse(
                    "Internal server error ",
                    HttpStatus.NOT_FOUND,
                    null
            );
        }
    }

    @PutMapping("/update")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> updateUser(@RequestBody User user) {
        List<User> tempUser = usersService.getIdUser(user.getPhoneNumber());
        if (tempUser.size() == 0) {
            return ResponseHandler.generateResponse(
                    "User with phone number " + user.getPhoneNumber() + " is not exist",
                    HttpStatus.NOT_FOUND,
                    null
            );
        }
        return usersService.getUsers(tempUser.get(0).getUserId())
                .map(userData -> {
                    userData.setFirstName(user.getFirstName());
                    userData.setLastName(user.getLastName());
                    userData.setAge(user.getAge());
                    usersService.updateUser(userData);
                    return ResponseHandler.generateResponse(
                            "Successfully update user ",
                            HttpStatus.OK,
                            userData
                    );
                })
                .orElse(ResponseHandler.generateResponse(
                        "Internal server error ",
                        HttpStatus.NOT_FOUND,
                        null
                ));
    }

    @PutMapping(
            path = "/updateForm",
            consumes = {
                    MediaType.APPLICATION_FORM_URLENCODED_VALUE,
                    MediaType.MULTIPART_FORM_DATA_VALUE
            },
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<?> update(@RequestBody @ModelAttribute User user) {
        List<User> tempUser = usersService.getIdUser(user.getPhoneNumber());
        if (tempUser.size() == 0) {
            return ResponseHandler.generateResponse(
                    "User with phone number " + user.getPhoneNumber() + " is not exist",
                    HttpStatus.NOT_FOUND,
                    null
            );
        }
        return usersService.getUsers(tempUser.get(0).getUserId())
                .map(userData -> {
                    userData.setFirstName(user.getFirstName());
                    userData.setLastName(user.getLastName());
                    userData.setAge(user.getAge());
                    userData.setPhoneNumber(user.getPhoneNumber());
                    usersService.updateUser(userData);
                    return ResponseHandler.generateResponse(
                            "Successfully update user ",
                            HttpStatus.OK,
                            userData
                    );
                })
                .orElse(ResponseHandler.generateResponse(
                        "Internal server error ",
                        HttpStatus.NOT_FOUND,
                        null
                ));
    }

    private List<User> checkUser(String phoneNumber) {
        List<User> data = usersService.getIdUser(phoneNumber);
        return data;
    }

    private HashMap mappingResponse(User data) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("firstName", data.getFirstName());
        map.put("lastName", data.getLastName());
        map.put("age", data.getAge());
        map.put("phoneNumber", data.getPhoneNumber());
        return map;
    }

    private Set<Roles> checkRole(Set<String> strRoles) {
        Set<Roles> roles = new HashSet<>();
        if (strRoles == null) {
            Roles userRole = roleRepository.findByName(EnumRoles.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
            return roles;
        }

        Roles adminRole = roleRepository.findByName(EnumRoles.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(adminRole);
        return roles;
    }
}
