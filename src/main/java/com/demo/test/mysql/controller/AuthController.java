package com.demo.test.mysql.controller;

import com.demo.test.mysql.exception.RefreshToken.RefreshTokenException;
import com.demo.test.mysql.model.EnumRoles;
import com.demo.test.mysql.model.RefreshToken;
import com.demo.test.mysql.model.Roles;
import com.demo.test.mysql.model.User;
import com.demo.test.mysql.payload.request.LoginRequest;
import com.demo.test.mysql.payload.request.RefreshTokenRequest;
import com.demo.test.mysql.payload.request.RegistrationRequest;
import com.demo.test.mysql.payload.response.JwtResponse;
import com.demo.test.mysql.payload.response.MessageResponse;
import com.demo.test.mysql.payload.response.RefreshTokenResponse;
import com.demo.test.mysql.repository.RoleRepository;
import com.demo.test.mysql.repository.UsersRepository;
import com.demo.test.mysql.service.RefreshTokenService;
import com.demo.test.mysql.service.UserDetailsImpl;
import com.demo.test.mysql.util.Jwt.JwtUtils;
import com.demo.test.mysql.util.RSA.RSAKeyProperties;
import com.demo.test.mysql.util.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private RSAKeyProperties rsaKeyProp;

    @Autowired
    RefreshTokenService refreshTokenService;

    @PostMapping("signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getPhoneNumber(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication, rsaKeyProp.getPrivateKey());
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
//        String jwt = jwtUtils.generateJwtToken(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

//        RefreshToken refreshToken = refreshTokenService.generateRefreshToken(userDetails.getUsername());

        return ResponseHandler.generateResponse(
                "Login success",
                HttpStatus.OK,
                new JwtResponse(
                        userDetails.getUserId(),
                        userDetails.getUsername(),
                        userDetails.getFirsName(),
                        userDetails.getLastName(),
                        userDetails.getAge(),
                        roles, jwt, "TEST REFRESH TOKEN")
        );
    }

    @PostMapping("/signUp")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegistrationRequest registrationRequest) {
        if (usersRepository.existsByPhoneNumber(registrationRequest.getPhoneNumber())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Phone number is already use!"));
        }

        // Create new user's account
        User user = new User(
                registrationRequest.getFirstName(),
                registrationRequest.getLastName(),
                registrationRequest.getAge(),
                registrationRequest.getPhoneNumber(),
                passwordEncoder.encode(registrationRequest.getPassword())
        );

        Set<String> strRoles = registrationRequest.getRole();
        Set<Roles> roles = new HashSet<>();

        if (strRoles == null) {
            Roles userRole = roleRepository.findByName(EnumRoles.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Roles adminRole = roleRepository.findByName(EnumRoles.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    default:
                        Roles userRole = roleRepository.findByName(EnumRoles.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        usersRepository.save(user);
        return ResponseHandler.generateResponse(
                "Successfully create user ",
                HttpStatus.OK,
                user
        );
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody RefreshTokenRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = jwtUtils.generateTokenFromUsername(user.getPhoneNumber(), rsaKeyProp.getPrivateKey());
                    return ResponseHandler.generateResponse(
                            "Login success",
                            HttpStatus.OK,
                            new RefreshTokenResponse(token,  requestRefreshToken)
                    );
                })
                .orElseThrow(() -> new RefreshTokenException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
