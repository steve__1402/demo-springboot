package com.demo.test.mysql.repository;

import com.demo.test.mysql.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
public interface UsersRepository extends JpaRepository<User, Integer> {

    @Query(value = "select u from User u where u.phoneNumber = ?1 ")
    List<User> findIdByPhoneNumber(String phoneNumber);

    @Query(value = "select u from User u where u.phoneNumber = ?1 ")
    Optional<User> findById(String id);

    @Query(value = "select u from User u where u.userId = ?1 ")
    Optional<User> findUserById(String id);

    @Query(value = "delete from User u where u.phoneNumber = ?1 ")
    void deleteByPhoneNumber(String id);

    Boolean existsByPhoneNumber(String phoneNumber);
}
