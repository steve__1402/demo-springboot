package com.demo.test.mysql.repository;

import com.demo.test.mysql.model.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long>{

    @Override
    Optional<RefreshToken> findById(Long id);

    Optional<RefreshToken> findByToken(String refreshToken);

}