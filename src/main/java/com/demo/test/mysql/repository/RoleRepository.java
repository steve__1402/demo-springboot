package com.demo.test.mysql.repository;

import com.demo.test.mysql.model.EnumRoles;
import com.demo.test.mysql.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Roles, Long> {
    Optional<Roles> findByName(EnumRoles name);
}
