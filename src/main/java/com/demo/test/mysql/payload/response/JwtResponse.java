package com.demo.test.mysql.payload.response;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {
    private String token;
    private String refreshToken;
    private String type = "Bearer";
    private String id;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private Integer age;
    private List<String> roles;

    public JwtResponse(String id, String phoneNumber,
                       String firstName, String lastName,
                       Integer age,List<String> roles,
                       String accessToken, String refreshToken) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.roles = roles;
        this.token = accessToken;
        this.refreshToken = refreshToken;
    }
}
